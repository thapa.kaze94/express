//number==.>1,2,3,NaN
// premitive 
let a=1
console.log(typeof a) //number
let name="sajan"
console.log(typeof name) //string

let b=false
console.log(typeof b) //boolean
let n=null
console.log(typeof n); //object
let u=undefined 
console.log(typeof u); //undefined
 //Non primitive
 //typeof non-premitive==>object
 let ar1=[1,2,]
console.log(typeof ar1)
let info={name:"sajan"}
console.log(typeof info)
let error=new Error()
console.log(typeof error)
let date=new Date()
console.log(typeof date)
let set=new Set()
console.log(typeof set)