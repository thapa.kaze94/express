//premitive=> string,boolean,null,undefined
//==== works
console.log(1===1);
console.log("sajan"==="sajan"); // returns true value

let a=2
let b=a
a=4
console.log(a===b)
console.log(b)
// Non-premitive =>array,object
//=== doesnot work
// console.log([1,2]===[1,2]); //==> always return false value since it doesnot have memory address
// in Non-premitive  memory space will be shared  if there is copy of elememt.
let ar1=[1,2]
let ar2=ar1 // ar1 shares memory with ar2
ar2.push(3)
console.log(ar1) //[1,2,3]
console.log(ar2)//[1,2,3]
console.log(ar1===ar2) //returns true if the memory address is same.
let ar3=[1,2]
console.log(ar1===ar3) // memory address is no same so it returns false  
let info1={name:"sajan"}
let info2=info1
console.log(info1===info2);

