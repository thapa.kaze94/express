let ar1=[1,true,...[1,2,3]]
console.log(ar1);
// spread operator==> ...  used in array and objects
// spread operator are the wrapper opener ...[1,2,3]==> 1,2,3
let ar2=[4,5,6]
let ar3=[...ar1,...ar2]
console.log(ar3)